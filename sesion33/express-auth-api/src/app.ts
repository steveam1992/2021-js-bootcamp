import express, { Request, Response } from "express";

import { authRouter } from "./routes/auth";
import { userS } from "./routes/user";


const app = express();
const port: number = 3000;

app.use(express.json());

app.get('/', (req: Request, res: Response)=>{
    res.send('Express auth API V1');
});

app.use('/auth',authRouter)
app.use('/users',userS)
app.listen(port, ()=>{
    console.log(`API at http://127.0.0.1:${port}`);
})