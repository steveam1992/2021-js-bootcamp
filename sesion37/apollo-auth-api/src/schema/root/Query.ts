import { gql } from "apollo-server";

export const Query = gql`
  type Query {
    getAllBooks: [Book]
    getBook(id: Int!): Book
    

    # -------------------------------
    login(email: String!, password: String! ): String
    getAllUsers: [User]
  }
`