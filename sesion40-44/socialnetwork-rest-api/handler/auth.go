package handler

import (
	"fmt"
	"net/http"
	"os"
	"socialnetwork-rest-api/data"
	"socialnetwork-rest-api/data/dao"
	"socialnetwork-rest-api/data/entity"
	"time"

	"github.com/dgrijalva/jwt-go"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"golang.org/x/crypto/bcrypt"
)

// LogIn ...
func LogIn(c echo.Context) error {

	user := new(entity.User)
	if err := c.Bind(user); err != nil {
		return err
	}
	conn, err := data.Open()
	if err != nil {
		return err
	}
	defer conn.Close()
	db := dao.DB{DB: conn}
	usr, err := db.FindUSerByEmail(user.Email)
	if err != nil {
		log.Error(err)
		return echo.ErrUnauthorized
	}
	fmt.Println(usr)
	fmt.Println(user)
	if err := bcrypt.CompareHashAndPassword([]byte(usr.Password), []byte(user.Password)); err != nil {
		return err
	}
	token := jwt.New(jwt.SigningMethodHS256)
	key := []byte(os.Getenv("JWT_SECRECT_KEY"))
	fmt.Println(key)
	claims := token.Claims.(jwt.MapClaims)
	claims["name"] = usr.Name
	claims["lastname"] = usr.LastName
	claims["email"] = usr.Email
	claims["is_admin"] = usr.IsAdmin
	claims["exp"] = time.Now().Add(3 * time.Minute).Unix()
	jwtoken, err := token.SignedString([]byte(os.Getenv("JWT_SECRECT_KEY")))

	if err != nil {
		log.Error(err)
		return err
	}
	fmt.Println(jwtoken)

	return c.JSONPretty(http.StatusOK, map[string]interface{}{
		"token": jwtoken,
	}, "")

}
