import { books } from "../db";


export const resolvers = {
    Query: {
        books: ()=> books,
        book: (_, { id }) => books.find(book => book.id === id),
        posts: async (_, __, { dataSources })=> await dataSources.postsAPI.getPosts(),
        post: async (_, { id },{ dataSources })=> await dataSources.postsAPI.getPost(id),
        
    },
    Mutation: {
        delPost: async (_, { id },{ dataSources })=> await dataSources.postsAPI.deletePost(id),
        crPost: async (_, { userId, title, body },{ dataSources })=> await dataSources.postsAPI.createPost({userId, title, body}),
        upPost: async (_, { id,userId, title, body },{ dataSources })=> await dataSources.postsAPI.updatePost(id, {userId, title, body})
         

    }
}