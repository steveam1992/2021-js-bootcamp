import express, { Request, Response } from "express";
import bcrypt from "bcrypt";
import jwt from "jsonwebtoken";
import { UserAPI } from "../db/api/UserAPI";
import { IUser } from "../model/IUser";
import { Connection } from "typeorm";
import { getdbConnection } from "../db";
require('dotenv').config()
export const authRouter = express.Router()

let connection: Connection
let userAPI: UserAPI
authRouter.post('/login', async (req: Request, res: Response)=>{
    const {email, password} = req.body;
    connection =  await getdbConnection();
    userAPI = new UserAPI(connection)
    const user = await userAPI.getUserByEmail(email)
    connection.close()
    if(user){

        const result = await bcrypt.compare(password, user.password)
        console.log(result)
        if(result){
            let token = jwt.sign({
                id:user.id,
                 name: user.name,
                }, process.env.JWT_SECRET, {expiresIn: 180 });
            res.send('accediste '+token)
        }else{
            res.send('contraseña invalida')

        }
    }else{

        res.send('no existe el usuario')
    }
});


authRouter.post('/singup', async (req: Request, res: Response)=>{
  const {name, email, password} = req.body;
  
    // hash es lo que se almacena en BD
    const user: IUser={
        name,
        email,
        password
    }
    connection =  await getdbConnection();
    user.password = await bcrypt.hash(password, 9);
    userAPI = new UserAPI(connection)
    const result = await userAPI.saveUser(user)
    connection.close()
    res.send(result)
})

