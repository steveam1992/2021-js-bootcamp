
import  {strings}  from "../src/strings";
import { templateStingTestCases } from "./testCases/strings_testCases";
let person ={
 name: 'Steve',
 lastname: 'Amezcua',
 role: 'admin'
}
test('template string', ()=>{
    // const res = "Bienvenido "+person.name+" "+person.lastname+", accediste como "+person.role
    const res =  `Bienvenido ${person.name} ${person.lastname}, accediste como ${person.role}!`
    const result = "Bienvenido Steve Amezcua, accediste como admin!"
    expect(res).toBe(result)
})



// test('html template string',()=>{
    
 

//     const entrada = strings(templateStingTestCases[0].entrada)
//     const salida = templateStingTestCases[0].salida
//     expect(entrada).toBe(salida)
    
// })

// reto: generalizar el codigo para todos


test('html template string',()=>{
    templateStingTestCases.forEach(elem =>{
        const entrada = strings(elem.entrada)
        const salida = elem.salida
        expect(entrada).toBe(salida)
    }) 
})