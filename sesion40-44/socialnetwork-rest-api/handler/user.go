package handler

import (
	"log"
	"net/http"
	"socialnetwork-rest-api/data"
	"socialnetwork-rest-api/data/dao"
	"socialnetwork-rest-api/data/entity"
	"strconv"

	"github.com/labstack/echo/v4"
)

// SaveUser ...
func SaveUser(c echo.Context) error {
	user := new(entity.User)
	if err := c.Bind(user); err != nil {
		return err
	}

	// opendaDB ...
	conn, err := data.Open()
	if err != nil {
		log.Fatal("unable to connect to DB: ", err)
	}
	defer conn.Close()
	db := dao.DB{DB: conn}
	db.InsertUser(*user)

	return c.JSONPretty(http.StatusCreated, "User Saved", "result:")

}

// GetUserByID ...
func GetUserByID(c echo.Context) error {

	idStr := c.Param("id")

	if idStr != "" {
		id, err := strconv.Atoi(idStr)
		if err != nil {
			return err
		}
		conn, err := data.Open()
		if err != nil {
			log.Fatal("unable to connect to DB: ", err)
		}
		defer conn.Close()
		db := dao.DB{DB: conn}
		user, err := db.FindUSerById(id)
		return c.JSONPretty(http.StatusOK, map[string]interface{}{
			"id":       user.ID,
			"name":     user.Name,
			"lastname": user.LastName,
			"email":    user.Email,
			"isAdmin":  user.IsAdmin,
		}, " ")
	}

	return c.String(http.StatusOK, "Wolcome ")
}

// UpdateUser ...
func UpdateUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "actualizado "+id)
}

// DeleteUser ...
func DeleteUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "eliminado "+id)
}
