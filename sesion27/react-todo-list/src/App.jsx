import { Todo } from "./view/Todo";
import './css/App.css';

export const App = () => {
  return (
    <div className="App">
      <Todo />
    </div>
  );
}
