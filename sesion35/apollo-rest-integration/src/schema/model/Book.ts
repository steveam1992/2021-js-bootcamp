import { gql } from "apollo-server";

export const Book = gql`
  type Book {
    id: Int
    title: String
    author: String
  }
`