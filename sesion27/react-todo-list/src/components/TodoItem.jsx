export const TodoItem = (props) => {
  const { id, title, completed } = props.todo
  const { markComplete, deleteTodo } = props
  const componentStyle = () => {
    return {
      background: completed ? 'gray' : 'none',
      margin: '6rem',
      textDecoration: completed ? 'line-through' : 'none',
    }
  }
  return (
    <>
      <input
        type="checkbox"
        checked={completed}
        onChange={e => markComplete(id, e)}  //¿cómo mandar el "id" en el evento? 🤔
      />
      <label style={componentStyle()}>{title}</label>
      <button onClick={e => deleteTodo(id, e)}>X</button>
    </>
  )
}
