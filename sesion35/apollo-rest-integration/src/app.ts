import { ApolloServer, gql } from "apollo-server";
import { PostsAPI } from "./datasouce/PostsAPI";
import { resolvers } from "./resolver";
import { typeDefs } from "./schema/index";


new ApolloServer({typeDefs, resolvers, dataSources:()=> {return {postsAPI: new PostsAPI}}})
.listen()
.then( ( {url} ) => {
    console.log(`🤘🏽 al putazo!! ${url} `);
    
})