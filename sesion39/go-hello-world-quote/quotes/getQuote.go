package quotes

import (
	"strings"

	"rsc.io/quote/v3"
)

func GetQuote(requiredQuete string) string {
	var result string

	switch strings.ToLower(requiredQuete) {
	case "concurrency":
		result = quote.Concurrency()
	case "glassv3":
		result = quote.GlassV3()
	case "gov3":
		result = quote.GoV3()
	case "hellov3":
		result = quote.HelloV3()
	case "optv3":
		result = quote.OptV3()
	}
	return result
}
