import { ApolloServer } from "apollo-server";
import { typeDefs } from "./schema";
import { resolvers } from "./resolver";
import { BookAPI } from "./datasource/BookAPI";
import { getDBConnection } from "./db";
import { Connection } from "typeorm";
let connection: Connection

(async () => {
  connection = await getDBConnection()
})()

new ApolloServer({typeDefs, resolvers, dataSources: ()=> {
    return{
        bookAPI: new BookAPI(connection)
    }
}})
.listen()
.then(({url})=>{
    console.log(`Server ready at ${url}`);
    
})