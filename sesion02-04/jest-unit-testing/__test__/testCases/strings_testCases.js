export const templateStingTestCases = [
        {
            entrada: {
                name: 'prueba',
                image_url: 'http://via.placeholder.com/300',
                desc: 'Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.'
                },
            salida: '<div class="product"><div class="product-image"><img alt="prueba" src="http://via.placeholder.com/300"></div><div class="product-desc">Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.</div></div>'
        },
        {
            entrada: {
                name: 'jose',
                image_url: 'http://via.placeholder.com/300',
                desc: 'Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.'
                },
            salida: '<div class="product"><div class="product-image"><img alt="jose" src="http://via.placeholder.com/300"></div><div class="product-desc">Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.</div></div>'
        },
        {
            entrada: {
                name: 'prueba 3',
                image_url: 'http://via.placeholder.com/300',
                desc: 'Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.'
                },
            salida: '<div class="product"><div class="product-image"><img alt="prueba 3" src="http://via.placeholder.com/300"></div><div class="product-desc">Croissant powder toffee jelly-o lollipop cookie cookie. Halvah cake gummi bears. Dessert lollipop topping tart jujubes powder oat cake.</div></div>'
        },
        
       

];