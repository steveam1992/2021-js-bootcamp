

    export const resolvers = {
        Query: {
          getAllBooks: async (_, __, { token,dataSources }) =>dataSources.authUser.verifyToken(token)
          &&  dataSources.bookAPI.getBooks(),
          getBook: async (_,{id},{ token,dataSources }) =>dataSources.authUser.verifyToken(token)
          && dataSources.bookAPI.getBook(id),
            // ------------------------------------------
            login: async (_, {email, password}, {dataSources}) => dataSources.authUser.getToken({email, password}),
            getAllUsers: (_, __, {dataSources}) => dataSources.userAPI.getAllUsers()

        },
        Mutation:{
          createBook: async (_,{input},{ dataSources })=> dataSources.bookAPI.createBook({...input}),

          updateBook: async (_,{id,input},{ dataSources })=> dataSources.bookAPI.updateBook({id, ...input}),

          deleteBook: async (_,{id},{ dataSources })=> dataSources.bookAPI.deleteBook(id),
          //  ----------------------------------------------
          register: async (_, { input }, {dataSources})=>dataSources.userAPI.register({...input})

        }
      }