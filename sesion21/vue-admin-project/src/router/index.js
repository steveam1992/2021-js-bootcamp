import Vue from "vue";
import VueRouter from 'vue-router';
// import Home from "../views/Home.vue";
// import About from "../views/About.vue";
// import Secure from "../views/Secure.vue";
// import Register from "../views/Register.vue";
// import Login from "../views/Login.vue";

Vue.use(VueRouter)

const router = new VueRouter({
  routes: [
    { path: '/', name: 'Home', component: ()=> import('../views/Home.vue') },
    { path: '/about', name: 'About', component: ()=> import('../views/About.vue') },
    { path: '/secure', name: 'Secure', component: ()=> import('../views/Secure.vue') },
    { path: '/register', name: 'Register', component: ()=> import('../views/Register.vue') },
    { path: '/login', name: 'Login', component: ()=> import('../views/Login.vue') },
  ]
})

export default router