import { gql } from "apollo-server";

export const Post = gql`
  type Post {
    id: Int
    userId: Int
    body: String
    title: String

  }
`