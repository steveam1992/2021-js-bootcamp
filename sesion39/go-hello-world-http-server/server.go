package main

import (
	"fmt"
	"net/http"
)

func main() {
	port := ":3000"
	http.HandleFunc("/", hello)
	http.HandleFunc("/steve", myHandler)

	http.ListenAndServe(port, nil)
}
func hello(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello world http server!\n")
}
func myHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "hello steve http server!\n")
}
