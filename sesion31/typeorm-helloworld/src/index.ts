import { Connection } from "typeorm";
import { getDBConnection } from "./db";
import { UserAPI } from "./db/api/UserAPI";
import { IUser } from "./model/IUser";

let connection: Connection
let userAPI: UserAPI
(async ()=>{
    connection =  await getDBConnection()
    userAPI  = new UserAPI(connection)
    const INSERT: IUser = {

        name: 'steve',
        email: 'example@ewg.com',
        password: 'querty',
        createdAt: new Date(),
        updatedAt: new Date()
    };
    await userAPI.seveUser(INSERT)
})()
