
import http from 'http';

const hostname: string = '127.0.0.1';
const port: number = 3000;
const server = http.createServer((req, res) => {
    switch (req.url) {
        case '/':
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            if (req.method === 'GET' || req.method === 'OPTIONS') {
                res.end('Hello World');
            }else{
                res.statusCode = 405;
                res.end();
            }
            break;
        case '/login':
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            if (req.method === 'POST') {
                res.end('Hello World');
            }else{
                res.statusCode = 405;
                res.end();
            }
            break;
        case '/users':
            res.statusCode = 200;
            res.setHeader('Content-Type', 'text/plain');
            if (req.method === 'POST' || req.method === 'GET' || req.method === 'PUT' || req.method === 'DELETE') {
                res.end('Hello World');
            }else{
                res.statusCode = 405;
                res.end();
            }
            break;

    }



});

server.listen(port, hostname, () => {
    console.log(`Server running at http://${hostname}:${port}/`);
});



