module.exports = (sequelize, DataTypes)=>{



class User extends sequelize.Sequelize.Model { }

User.init({
  id: {
    type: DataTypes.INTEGER,
    primaryKey: true,
    autoIncrement: true
  },
  name: {
      type: DataTypes.TEXT,
      allowNull: false
  },
  email: {
    type: DataTypes.TEXT,
    unique: true
  },
  password: {
      type: DataTypes.TEXT
  },
  createdAt: {
    field: 'created_at',
    type: DataTypes.DATE
  },
  updatedAt: {
    field: 'updated_at',
    type: DataTypes.DATE
  }
},{
     // Other model options go here
  sequelize, // We need to pass the connection instance
  modelName: 'User', // We need to choose the model name
  freezeTableName: true

})

 return User;
}