import { DataSource } from "apollo-datasource"
import { Connection } from "typeorm"
import { Book } from "../db/entity/Book"
import { IBook } from "../types"


export class BookAPI extends DataSource {

  private connection: Connection

  constructor(connection: Connection) {
    super()
    this.connection = connection
  }

  async getBooks(): Promise<Array<IBook>> {
    return await this.connection.manager.find(Book)
  }

  async getBook(id: number): Promise<IBook>{
    return await this.connection.manager.findOne(Book,{
      where: {id}
  })
  }
  async createBook(book): Promise<IBook>{
    return await this.connection.manager.save(Book,book)
    
  }
  async deleteBook(id: number): Promise<IBook>{
    let currentBook = await this.connection.manager.findOne(Book,id)
    return await this.connection.manager.remove(currentBook)
    
  }
  async updateBook(book: IBook): Promise<IBook>{
    let currentBook: Book = await this.connection.manager.findOne(Book,book.id)
    Object.keys(book).forEach(key =>book[key] === undefined && delete book[key])
    currentBook = { ...currentBook, ...book }
    return await this.connection.manager.save(Book,currentBook)
    
  }
}
