import { gql } from "apollo-server";

export const Book = gql`
    type Book {
        id: Int
        title: String
        author: String
        publisher: String
        ISBN10: String
        ISBN13: String
        category: String
        year: Int
        language: String
        totalPages: Int
    }
   input BookInput {
        title: String
        author: String
        publisher: String
        ISBN10: String
        ISBN13: String
        category: String
        year: Int
        language: String
        totalPages: Int
    }
`