
    export const resolvers = {
        Query: {
          books: async (_, __, { dataSources }) => dataSources.bookAPI.getBooks(),
          book: async (_,{id},{ dataSources })=> dataSources.bookAPI.getBook(id),
        },
        Mutation:{
          createBook: async (_,{input},{ dataSources })=> dataSources.bookAPI.createBook({...input}),

          updateBook: async (_,{id,input},{ dataSources })=> dataSources.bookAPI.updateBook({id, ...input}),

          deleteBook: async (_,{id},{ dataSources })=> dataSources.bookAPI.deleteBook(id)

        }
      }