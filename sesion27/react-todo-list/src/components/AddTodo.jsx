import { useState } from 'react';

export const AddTodo = (props) => {
  const [title, setTitle] = useState('')

  const handleSubmit = e => {
    e.preventDefault();
    console.log('*** handleSubmit ***');
    setTitle('')
    props.addTodo(title)
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <input
          type="text"
          name="addTodo"
          placeholder="Add ToDo task..."
          value={title} onChange={e => setTitle(e.target.value)}
        />
        <button type="submit">Add</button>
      </div>
    </form>)
}