package handler

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"os"

	"github.com/labstack/echo/v4"
)

// GetPosts ...
func GetPosts(c echo.Context) error {
	resp, err := http.Get(os.Getenv("TYPICODE_SERVER_URL") + "/posts")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	fmt.Println("Response status:", resp.Status)

	data, _ := ioutil.ReadAll(resp.Body)
	bodyParse := string(data)

	return c.String(http.StatusOK, bodyParse)

}
