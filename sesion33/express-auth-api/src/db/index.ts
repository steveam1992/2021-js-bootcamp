import { createConnection } from "typeorm";
var __dirname

export const getdbConnection = async ()=>{
    const connnection = await createConnection({
        type: 'sqlite',
        database: 'database.sqlite',
        synchronize: true,
        logging: true,
        entities: [
            `${__dirname}/entity/*`
        ]
    })
    return connnection
}