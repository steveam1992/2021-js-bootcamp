import { Request, Response } from "express"
import express  from 'express'
import { userRouter } from "./router/user"
const app = express()
const port: number = 3000

app.use(express.json())
app.use('/user', userRouter)

app.get('/', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path`)
}).options('/', (req: Request, res:  Response)=>{
    console.log(req);
    res.send(`[${req.method}] Root path`)
});

app.post('/login', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path /login [${JSON.stringify(req.body)}]`)
});

app.post('/user', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path`)
}).get('/user', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path`)
}).put('/user', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path`)
}).delete('/user', (req: Request, res:  Response) => {
    console.log(req);
    
    res.send(`[${req.method}] Root path`)
})

app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})