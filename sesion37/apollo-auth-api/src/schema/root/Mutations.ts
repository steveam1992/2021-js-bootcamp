import { gql } from "apollo-server";

export const Mutation = gql`
  type Mutation {
    # createBook(title: String!, author: String!, publisher: String!, ISBN10: String, ISBN13: String, category: String, year: Int, language: String!, totalPages: Int): Book
    createBook(input: BookInput): Book
    updateBook(id:Int!,input: BookInput): Book
    deleteBook(id: Int!): Book

    # -----------------------
    register(input: UserInput): User
  }
`