import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    status:'',
    token: localStorage.getItem('token') || '',
    user: {}
  },
  getters:{
    isLoggedIn: state => !!state.token
  },
  mutations: {
    auth_success(state, {token, user}){
      state.status ='success'
      state.token = token
      state.user = user

    },
    auth_err(state){
      state.status = 'error'
    },
    logout(state){
      state.status = '',
      state.token = '',
      state.user = {}
    },
    signup(state){
      state.status = '',
      state.token = '',
      state.user = {}
    }
  },
  actions: {
   async login({commit},loginData){
        try {
          const res = await fetch('http://127.0.0.1:1323/login',{
            method: 'POST',
            headers:{
              'Content-Type': 'application/json; charset=UTF-8'
            },
            body: JSON.stringify(loginData)
          })
          if (res.status === 401) {
            commit('auth_err')
            localStorage.removeItem('token')
          }
          const  data = await res.json()
          const token = data.token
          const Duser = JSON.parse(atob(token.split('.')[1]))
          const user = {
            email: Duser.email,
            name: Duser.name,
            lastname: Duser.lastname,
            isAdmin: Duser.is_admin

          }
          console.log(user);

          localStorage.setItem('token', token)

          commit('auth_success', {token, user})
        } catch (error) {
          commit('auth_err')
          localStorage.removeItem('token')
        }
      
    },
    async logout({commit}){
     await commit('logout')
     await localStorage.removeItem('token')
    },
   async signup({commit},register){
        const res = await fetch('http://127.0.0.1:1323/user',{
          method: 'POST',
          headers:{
            'Content-Type': 'application/json; charset=UTF-8'
          },
          body: JSON.stringify(register)
        })
        if (res.status === 401) {
          console.log('error al registrar');
          
        }
        commit('signup')
        
     
    }
  },
  modules: {
  }
})
