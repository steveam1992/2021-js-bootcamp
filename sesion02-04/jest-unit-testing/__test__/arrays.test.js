import { getArrayFrom, getArrayOf } from "../src/arrays";
import { arrayFroms, arrayOf } from "./testCases/array_testCases";


// Array.from
// Array.of
// Array.prototype.fill
// Array.prototype.includes
// Array.prototype.find
arrayFroms.forEach(element => {
  test('Array.from test cases', () => {
      // console.log(Array.from(1, 2, 3)); //esta falla
       let entrada = getArrayFrom(element.entrada)
       let salida = element.salida
       expect(entrada).toEqual(salida)
    })
});
arrayOf.forEach(element=>{
  test('Array.of test cases', () => {
    let entrada = getArrayOf(element.entrada)
    let salida  =element.salida
    expect(entrada).toEqual(salida)
   
  })
})
  // test('Array.fill test cases', () => {
  //   console.log(new Array(9).fill(1, 2, 3));
  //   console.log(new Array(9).fill(1, 2, 4));
  //   console.log(new Array(9).fill(1, 2, 5));
  //   console.log(new Array(9).fill('x', 3, 6));
  //   return
  // })
  // test('Array.find test cases', () => {
  //   // devuelve solo al primero que cumpla el predicado
  //   console.log([1, 2, 3, 4, 5, 6].find(n => n % 2));
  //   console.log([1, 2, 3, 4, 5, 6].find(n => n > 9)); //undefined
  //   return
  // })
  // test('Array.filter test cases', () => {
  //   // devuelve todos los que cumplen con el predicado
  //   console.log([1, 2, 3, 4, 5, 6].filter(n => n % 2));
  //   return
  // })