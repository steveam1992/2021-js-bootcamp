package main

import (
	"fmt"

	"go-hello-world-quote/quotes"
)

func main() {
	const ContString = "Hello World"
	fmt.Println(ContString)

	var result string
	result = quotes.GetQuote("concurrency")
	fmt.Println(result)
	result = quotes.GetQuote("glassv3")
	fmt.Println(result)
	result = quotes.GetQuote("gov3")
	fmt.Println(result)
	result = quotes.GetQuote("hellov3")
	fmt.Println(result)
	result = quotes.GetQuote("optv3")
	fmt.Println(result)

}
