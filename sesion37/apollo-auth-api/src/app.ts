require('dotenv').config();
import { ApolloServer } from "apollo-server";
import { typeDefs } from "./schema";
import { resolvers } from "./resolver";
import { BookAPI } from "./datasource/BookAPI";
import { getDBConnection } from "./db";
import { Connection } from "typeorm";
import { UserAPI } from "./datasource/UserAPI";
import { AuthUser } from "./datasource/authAPI";
let connection: Connection

(async () => {
  connection = await getDBConnection()
})()

new ApolloServer({typeDefs, resolvers, dataSources: ()=> {
    return{
        bookAPI: new BookAPI(connection),
        userAPI: new UserAPI(connection),
        authUser: new AuthUser(connection)
    }
},
context: async ({req})=>{
    return {
        token: req.headers.authorization || ''
    }
}
})
.listen()
.then(({url})=>{
    console.log(`Server ready at ${url}`);
    
})