package handler

import (
	"net/http"

	"github.com/labstack/echo/v4"
)

// SaveUser...
func SaveUser(c echo.Context) error {
	return c.String(http.StatusOK, "User Saved")
}

// login...
func LogIn(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "Wolcome "+id)
}

// UpdateUser
func UpdateUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "actualizado "+id)
}

// DeleteUser...
func DeleteUser(c echo.Context) error {
	id := c.Param("id")
	return c.String(http.StatusOK, "eliminado "+id)
}
