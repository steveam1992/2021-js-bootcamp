cat ~/.aws/config
cat ~/.aws/credentials

## make bucket:
aws s3 mb s3://steve-cli-bucket-test
## copy to bucket: 
aws s3 cp test.txt s3://steve-cli-bucket-test
### download

aws s3 cp s3://steve-cli-bucket-test ./tmp --include "*.txt" --recursive
### copy entre bucket

aws s3 cp s3://steve-cli-bucket-test s3://steve-myfirst-bucket --include "*.txt" --recursive



