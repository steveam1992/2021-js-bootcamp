require('dotenv').config()
const jwt = require('jsonwebtoken');

var token = jwt.sign({
    id: 2,
     name: 'steve',
     isAdmin: true
    }, process.env.JWT_SECRET, {expiresIn: 60 });


console.log(token)


jwt.verify(token, process.env.JWT_SECRET, (err, decoded) => {

    if(err){
        console.log('invalid token: ', err);

    }else{
        console.log('Success!! ', decoded);
    }
})