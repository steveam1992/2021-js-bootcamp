const n1 = document.querySelector('#primer_numero')
const n2 = document.querySelector('#segundo_numero')
const suma = document.querySelector('#A1')
const division = document.querySelector('#A2')
const resta = document.querySelector('#A3')
const mult = document.querySelector('#A4')
const resultNode = document.querySelector('#result')



suma.addEventListener('click', () => {
    let numero1 = parseInt(n1.value)
    let numero2 = parseInt(n2.value)
    let resultado = numero1 + numero2
    const textContent = document.createTextNode(resultado)
    const span = document.createElement('span')
    span.appendChild(textContent)

    limpiar()
    resultNode.appendChild(span)
    return result
})

division.addEventListener('click', () => {
    let numero1 = parseInt(n1.value)
    let numero2 = parseInt(n2.value)
    let resultado = numero1 / numero2
    const textContent = document.createTextNode(resultado)
    const span = document.createElement('span')
    span.appendChild(textContent)
    limpiar()
   
    resultNode.appendChild(span)
    return result
})

resta.addEventListener('click', () => {
    let numero1 = parseInt(n1.value)
    let numero2 = parseInt(n2.value)
    let resultado = numero1 - numero2
    const textContent = document.createTextNode(resultado)
    const span = document.createElement('span')
    span.appendChild(textContent)

    limpiar()
    resultNode.appendChild(span)
    return result
})

mult.addEventListener('click', () => {
    let numero1 = parseInt(n1.value)
    let numero2 = parseInt(n2.value)
    let resultado = numero1 * numero2
    const textContent = document.createTextNode(resultado)
    const span = document.createElement('span')
    span.appendChild(textContent)

    limpiar()
    resultNode.appendChild(span)
    return result
})

const limpiar = ()=>{
    if (resultNode.childNodes.length > 3) {
        resultNode.removeChild(resultNode.childNodes[3])
    }

}