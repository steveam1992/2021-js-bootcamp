export interface IBook {
    id: number,
    title: string,
    author: string,
    publisher: string,
    ISBN10: string,
    ISBN13: string,
    category: string,
    year: number,
    language: string,
    totalPages: number
  }
export interface IUser {
    id?: number,
    name: string,
    lastName: string,
    email: string,
    password: string,
    isAdmin: boolean
  
  }