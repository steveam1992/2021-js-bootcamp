const bcrypt = require('bcrypt')
const logger = require('../logger')

const saltRounds = 9;
const myPlaintextPassword = 's0/\/\P4$$w0rD'
const someOtherPlaintextPassword = 'not_bacon'

// const hash = '$2b$09$7x47o/OoGrFtiahM0xKh4OeHucVJ55AuMgw.3upHy6neAULT5Hm1S'
const hashArray =[
  
  '$2b$09$7x47o/OoGrFtiahM0xKh4OeHucVJ55AuMgw.3upHy6neAULT5Hm1S',
  '$2b$09$pLI2WLfclK1W.uz.IiLQnuZVF1PHFUN/2z8lmJPtSy0KQTwIMPjZu',
  '$2b$09$SrTkEOIOvwXBZejAfg.yzuVs3zKCndBOnMYB/PaG4.z/NQ87yycJC',
  '$2b$09$hV7U7Fi2xOS/g.vw2pZvCO.hv1.VpbajTBzHuqbgrXDnno.KJzKLO'
] 
// Generación de hash para almacenar en BD
// bcrypt.hash(myPlaintextPassword, saltRounds)
//   .then((hash) => {
//     // hash es lo que se almacena en BD
//     logger.debug(`myPlaintextPassword : ${hash}`)
//   })

// Carga el hash del password desde tu BD.
hashArray.forEach(hash=>{

  bcrypt.compare(myPlaintextPassword, hash)
  .then(function (result) {
    // result == true
    logger.debug(`myPlaintextPassword : ${result}`)
  });
  bcrypt.compare(someOtherPlaintextPassword, hash)
  .then(function (result) {
    // result == false
    logger.debug(`someOtherPlaintextPassword : ${result}`)
  });
})