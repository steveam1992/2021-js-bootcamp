import { gql } from "apollo-server";

export const Mutation = gql`
  type Mutation {
    crPost(userId: Int, title: String, body: String): Post
    upPost(id: Int!, userId: Int, title: String, body: String): Post
    delPost(id: Int!): Post
  },
  
`