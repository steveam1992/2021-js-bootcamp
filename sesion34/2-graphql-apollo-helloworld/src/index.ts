import { ApolloServer, gql } from "apollo-server";

interface IBook {
    id: number,
    title: String,
    author: String
}

const books: Array<IBook>=[
    {
        id: 1,
        title: "CELL",
        author: "Stepen king"
    },
    {
        id: 2,
        title: "IT",
        author: "Stepen king"
    },
    {
        id: 3,
        title: "Dark tower",
        author: "Stepen king"
    },
]

const typeDefs = gql`
 type Book {
     id: Int
     title: String
     author: String
 }
 type Query{
     books: [Book]
     book(id: Int): Book
 }
`
const resolvers = {
    Query: {
        books: ()=> books,
        book: (_, { id }) => books.find(book => book.id === id)
    }
}

new ApolloServer({typeDefs, resolvers})
.listen()
.then( ( {url} ) => {
    console.log(`🤘🏽 al putazo!! ${url} `);
    
})