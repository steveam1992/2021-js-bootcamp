const {Sequelize} = require('sequelize');

const sequelize = new Sequelize({
    dialect: 'sqlite',
    storage: './database.sqlite'
});


(async ()=>{
    try {
        await sequelize.authenticate();
        console.log('Connection has been established successfully.');
        
        // importamos modelo
        const User = require('./entity/User')(sequelize, Sequelize.DataTypes)
        
            // +++++ Create a new user ++++++++
            // const prueba = await User.create({ name: "Jane", email: "exampleœ@elel.com", password: "qwerty" });
            // console.log("Jane's auto-generated ID:", prueba.id);
            console.log(await User.findOne({
                    where: {
                        name: "Jane"
                    }
            }))
    } catch (error) {
        console.error('Unable to connect to the database:', error);
    }
})()