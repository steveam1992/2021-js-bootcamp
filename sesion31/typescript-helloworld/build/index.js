var helloWorld = 'Hello World';
var user = {
    id: 0,
    name: 'Hayes',
    isAdmin: true
};
var UserAccount = (function () {
    function UserAccount(name, id) {
        this.id = id;
        this.name = name;
    }
    return UserAccount;
})();
var userAcc = new UserAccount("Murphy", 1);
