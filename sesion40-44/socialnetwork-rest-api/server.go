package main

import (
	"log"
	"net/http"
	"socialnetwork-rest-api/handler"

	"github.com/joho/godotenv"
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/mattn/go-sqlite3"
)

func main() {

	if err := godotenv.Load(); err != nil {
		log.Fatal("Error loading .env file")
	}

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowHeaders: []string{echo.HeaderOrigin, echo.HeaderContentType, echo.HeaderAccept},
	}))

	// Routes
	e.GET("/", hello)
	//groups
	postGroup := e.Group("/post")
	e.POST("/login", handler.LogIn)
	postGroup.GET("", handler.GetPosts)
	// -------------------
	userGroup := e.Group("/user")
	userGroup.POST("", handler.SaveUser)
	userGroup.GET("/:id", handler.GetUserByID)
	userGroup.PUT("/:id", handler.UpdateUser)
	userGroup.DELETE("/:id", handler.DeleteUser)
	// Start server
	e.Logger.Fatal(e.Start(":1323"))

}

// Handler
func hello(c echo.Context) error {
	return c.String(http.StatusOK, "Social Network REST API!")
}
