export const getArrayFrom = (dato) =>{
    const arreglo = Array.from(dato, x => x + x)
    return arreglo;
}

export const getArrayOf = (dato)=>{
    const arregloOf = Array.of(dato)
    return arregloOf;
}