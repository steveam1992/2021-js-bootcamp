import { RESTDataSource } from "apollo-datasource-rest";


export class PostsAPI extends RESTDataSource{
    
    constructor(){
        super()
        this.baseURL= 'https://jsonplaceholder.typicode.com'
    }
    
    // 
    async getPosts(){
        return await this.get(`/posts`)
    }
    async getPost(id: number){
        return await this.get(`/posts?id=${id}`)

    }
    async createPost(post){
        return await this.post(`/posts/`, post)
    }
    async updatePost(id: number, post){
            return await this.patch(`/posts/${id}`, post)
    }

    async deletePost(id: number){
        return await this.delete(`/posts/${id}`)
    }
}