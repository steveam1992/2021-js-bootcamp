export function strings(dato) {
    const html =`<div class="product"><div class="product-image"><img alt="${dato.name}" src="${dato.image_url}"></div><div class="product-desc">${dato.desc}</div></div>`;
    return html;
}



// TODO: Ejercicio fuera de clase
// Probar y crear pruebas unitarias para los siguientes métodos de String

// String.prototype.startsWith
// String.prototype.endsWith
// String.prototype.includes
// String.prototype.repeat
// String.prototype.padStart
// String.prototype.padEnd


