package dao

import (
	"fmt"
	"socialnetwork-rest-api/data/entity"

	"golang.org/x/crypto/bcrypt"
)

// InsertUser saves a user on the DB
func (db *DB) InsertUser(user entity.User) (*entity.User, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword(
		[]byte(user.Password),
		bcrypt.DefaultCost,
	)
	if err != nil {
		return nil, err
	}
	fmt.Println(hashedPassword)
	user.Password = string(hashedPassword)
	fmt.Println("[InsertUser]", user)
	sqlStatemant := `INSERT INTO user(name, lastname, email, password, is_admin) VALUES ($1, $2, $3, $4, $5)`
	result, err := db.DB.Exec(sqlStatemant,
		user.Name,
		user.LastName,
		user.Email,
		user.Password,
		user.IsAdmin,
	)
	if err != nil {
		return nil, err
	}
	fmt.Println("sql result: ", result)
	return &user, nil
}

// FindUSerById ...
func (db *DB) FindUSerById(id int) (*entity.User, error) {
	var user entity.User
	if err := db.DB.QueryRow("SELECT id, name, lastname, email, is_admin FROM user WHERE id = $1", id).Scan(
		&user.ID,
		&user.Name,
		&user.LastName,
		&user.Email,
		&user.IsAdmin,
	); err != nil {
		return nil, err
	}
	return &user, nil

}

// FindUSerByEmail ...
func (db *DB) FindUSerByEmail(email string) (*entity.User, error) {
	var user entity.User

	if err := db.DB.QueryRow("SELECT * FROM \"user\" WHERE email = $1", email).Scan(
		&user.ID,
		&user.Name,
		&user.LastName,
		&user.Email,
		&user.Password,
		&user.IsAdmin,
	); err != nil {
		return nil, err
	}
	return &user, nil

}
