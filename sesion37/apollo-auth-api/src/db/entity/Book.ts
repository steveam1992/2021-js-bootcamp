import { Column, Entity, PrimaryGeneratedColumn } from "typeorm";

@Entity()
export class Book {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    author: string;

    @Column()
    publisher: string;

    @Column({name: 'ISBN_10', unique: true, nullable: true})
    ISBN10: string;

    @Column({name: 'ISBN_13', unique: true,nullable: true})
    ISBN13: string;

    @Column({nullable: true})
    category: string;

    @Column({nullable: true})
    year: number;

    @Column()
    language: string;

    @Column({name: 'total_pages', nullable: true})
    totalPages: number;

}