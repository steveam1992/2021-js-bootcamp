import { useEffect, useState } from "react"
import { AddTodo } from "../components/AddTodo"
import { FilterTodo } from "../components/FilterTodo"
import { TodoList } from "../components/TodoList"

export const Todo = () => {

  let [todos, setTodos] = useState([])
  let [counter, setCounter] = useState(0)

  useEffect(async () => {
    // setTodos([
    //   { id: 0, title: "aprender variables de estado en react", completed: false },
    //   { id: 1, title: "aprender renderizado condicional", completed: false },
    //   { id: 2, title: "aprender renderizado de listas", completed: true },
    // ])
  
   
   const response = await fetch('https://jsonplaceholder.typicode.com/todos?_limit=10');
   const jsonData = await  response.json()
   setTodos(jsonData)
   
  }, [])
  
  useEffect(() => { setCounter(todos.length)})
    const addTodo = (title) => {
      setTodos([...todos, {
          id: counter,
          title,
          complete: false
        }])
  }
  const deleteTodo = (id) =>{
  
    setTodos(todos.filter(t => t.id !== id))
  }
  const markComplete = id => {
    setTodos(todos.map(todo => {
      if (todo.id === id) todo.completed = !todo.completed
      return todo
    }))
  }

  return (
    <>
      <h3>Todo component</h3>
      <AddTodo addTodo={addTodo} />
      <TodoList todos={todos} markComplete={markComplete}  deleteTodo={deleteTodo}/>
      <FilterTodo />
    </>
  )
}