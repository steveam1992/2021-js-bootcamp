import logo from '../img/logo.svg';
import '../css/App.css';
import {Header} from '../components/Header';
import {Footer} from '../components/Footer';


export const App = ()=> {
  return (
    <div className="App">
        <Header/>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
         hola
        </a>
      </header>
      <Footer/>
    </div>
  );
}

