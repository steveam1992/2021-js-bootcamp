
var ctx = document.querySelector('#myChart').getContext('2d');
var fuente = document.querySelector('#fuentes').getContext('2d');
       
         var dynamicColors = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ",.3)";
        };
        var dynamicBorder = function() {
            var r = Math.floor(Math.random() * 255);
            var g = Math.floor(Math.random() * 255);
            var b = Math.floor(Math.random() * 255);
            return "rgb(" + r + "," + g + "," + b + ")";
         };

         
var chart = new Chart(ctx, {
    // The type of chart we want to create
    type: 'line',
    data: {
        labels: ['ENERO', 'FEBRERO', 'MARZO', 'ABRIL', 'MAYO', 'JUNIO'],
        datasets: [{
            label: 'Año Actual',
            backgroundColor:  dynamicColors(),
            borderColor: dynamicBorder(),
            borderWidth: 2,
            data: [50000, 30000, 90000, 5000, 18000, 60000, ]
        }
        ,{
            label: 'Año Anterior',
            backgroundColor:  dynamicColors(),
            borderColor: dynamicBorder(),
            data: [100000, 45000, 50000, 2000, 20000, 30000],
            type: 'line',
        }
    ]
    },

    // Configuration options go here
    options: {}
});


var chart = new Chart(fuente, {
    // The type of chart we want to create
    type: 'bar',
    data: {
        labels: ['LinkedIn', 'Facebook', 'Trafico directo','Instagram'],
        datasets: [{
            label: 'fuentes',
            backgroundColor:  dynamicColors(),
            borderColor: dynamicBorder(),
            borderWidth: 2,
            data: [5000, 3000, 9000, 5000]
        }
      
    ]
    },

    // Configuration options go here
    options: {
        scales: {
            xAxes: [{
                stacked: true
            }],
            yAxes: [{
                stacked: true
            }]
        }
    }
});