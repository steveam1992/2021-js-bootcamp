const path = require('path')

const {CleanWebpackPlugin} = require('clean-webpack-plugin')
const HtmlWebpackPlugin =require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')
const CssMinimizerWebpackPlugin = require('css-minimizer-webpack-plugin')


module.exports={
    entry: './src/index.js',
    output: {
        path: path.resolve(__dirname, '../dist'),
        filename: '[name].[contenthash].bundle.js'
    },
    plugins: [
        new CleanWebpackPlugin(), 
        new MiniCssExtractPlugin({
            filename: '[name].[contenthash].css'
        }),
        new HtmlWebpackPlugin({template: './src/index.html'})
    ],
    module:{
        rules: [
            {
                test: /\.css$/i,
                use: [ MiniCssExtractPlugin.loader, "css-loader"],
              },
              {
                  test: /\.scss$/,
                  use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader']
              }
        ]
    },
    optimization:{
        minimize: true,
        minimizer: [
          new CssMinimizerWebpackPlugin()
        ]
    }
}