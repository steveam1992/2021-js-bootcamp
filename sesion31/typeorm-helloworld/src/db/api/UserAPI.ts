import { Connection } from "typeorm";
import { IUser } from "../../model/IUser";
import { User } from "../entity/User";

export class UserAPI {
    private connection: Connection;
    constructor(connection: Connection){
        this.connection = connection
    }
    async seveUser(user: IUser): Promise<IUser>{
        
        return await this.connection.manager.save(User, user)
    }
}