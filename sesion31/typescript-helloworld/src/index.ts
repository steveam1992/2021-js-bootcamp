let helloWorld = 'Hello World';
interface User {
    id: number,
    name: string,
    isAdmin: boolean
}


const user: User = {
    id: 0,
    name: 'Hayes',
    isAdmin: true
}



class UserAccount {
    id: number;
    name: string;
    isAdmin: boolean;
  
    constructor(name: string, id: number) {
      this.id = id;
      this.name = name;
    }
  }
  
  const userAcc: User = new UserAccount("Murphy", 1);